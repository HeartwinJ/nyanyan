import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:http/http.dart' as http;
import 'globals.dart' as globals;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Nya Nyan',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String curPicUrl;

  Future _getPic() async {
    var response = await http.get(globals.apiUrl);
    var jsonData = jsonDecode(response.body);
    return jsonData['file'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  'Nyaaaaa!!!',
                  style: TextStyle(fontSize: 48),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: FutureBuilder(
                    future: _getPic(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.hasData) {
                          curPicUrl = snapshot.data;
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              image: DecorationImage(
                                image: NetworkImage(snapshot.data),
                              ),
                            ),
                          );
                        }
                      }
                      return Container(
                        alignment: Alignment.center,
                        child: Text('Loading...'),
                      );
                    },
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.refresh_rounded,
                        size: 40,
                      ),
                      onPressed: () {
                        print('Refresh button pressed!');
                        setState(() {});
                      },
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.share_rounded,
                        size: 40,
                      ),
                      onPressed: () async {
                        print('Share button pressed!');
                        var request =
                            await HttpClient().getUrl(Uri.parse(curPicUrl));
                        var response = await request.close();
                        Uint8List bytes =
                            await consolidateHttpClientResponseBytes(response);
                        await Share.file(
                            'ESYS AMLOG', 'nya.jpg', bytes, 'image/jpg');
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
